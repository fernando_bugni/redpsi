exports.branches = [ 
	{type: 'Análisis reichiano'},
	{type: 'Análisis transaccional (AT)'},
	{type: 'Arteterapia'},
	{type: 'Bioenergética'},
	{type: 'Coaching'},
	{type: 'Constelaciones familiares'},
	{type: 'EMDR'},
	{type: 'Focusing'},
	{type: 'Hipnosis clínica'},
	{type: 'Informes periciales'},
	{type: 'Logoterapia'},
	{type: 'Musicoterapia'},
	{type: 'Neuropsicología'},
	{type: 'Programación neurolingüística (PNL)'},
	{type: 'Psicoanálisis'},
	{type: 'Psicoanálisis lacaniano'},
	{type: 'Psicoanálisis línea IPA'},
	{type: 'Psicodrama'},
	{type: 'Psicología Forense'},
	{type: 'Psicología forense (jurídica)'},
	{type: 'Psicología infantil'},
	{type: 'Psicología positiva'},
	{type: 'Psicología transpersonal'},
	{type: 'Psicomotricidad'},
	{type: 'Psicoterapia Integrativa'},
	{type: 'Psicoterapia psicoanalítica'},
	{type: 'Risoterapia'},
	{type: 'Sexología'},
	{type: 'Terapia breve estratégica'},
	{type: 'Terapia breve psicoanalítica'},
	{type: 'Terapia centrada en el cliente'},
	{type: 'Terapia centrada en soluciones'},
	{type: 'Terapia cognitivo-conductual'},
	{type: 'Terapia con eneagrama'},
	{type: 'Terapia con yoga-meditación'},
	{type: 'Terapia constructivista'},
	{type: 'Terapia de aceptación y compromiso (ACT)'},
	{type: 'Terapia de interacción recíproca'},
	{type: 'Terapia de regresión'},
	{type: 'Terapia energética - emocional'},
	{type: 'Terapia gestalt'},
	{type: 'Terapia humanista'},
	{type: 'Terapia junguiana'},
	{type: 'Terapia racional-emotiva'},
	{type: 'Terapia sistémica/familiar'},
	{type: 'Terapia transpersonal'} 
]

exports.patiente_age = [
	{type: 'Niños'},
	{type: 'Adolescentes y jóvenes'},
	{type: 'Adultos'},
	{type: 'Mayores'} 
]

exports.patiente_type = [
	{type: 'Individual'},
	{type: 'Parejas'},
	{type: 'Familiar'},
	{type: 'Grupal'}
]

exports.treatment = [
	{type: 'Alcoholismo, Drogodependencias'},
	{type: 'Angustia'},
	{type: 'Anorexia, Bulimia y Obesidad'},
	{type: 'Ansiedad, Estrés'},
	{type: 'Autismo'},
	{type: 'Bullying'},
	{type: 'Conductas Adictivas, Ludopatía'},
	{type: 'Conductas agresivas, violencia'},
	{type: 'Dependencia emocional'},
	{type: 'Depresión'},
	{type: 'Dificultades adopción'},
	{type: 'Dificultades de Estudio, Técnicas'},
	{type: 'Dificultades del Aprendizaje'},
	{type: 'Disfonías'},
	{type: 'Disfunciones sexuales'},
	{type: 'Duelos y pérdidas'},
	{type: 'Educación Hijos, Orientación Profesional'},
	{type: 'Enfermedades psicosomáticas'},
	{type: 'Esquizofrenia (Psicosis)'},
	{type: 'Estimulación Temprana'},
	{type: 'Estrés Postraumático'},
	{type: 'Fibromialgia - Fatiga crónica'},
	{type: 'Fobias'},
	{type: 'Hiperactividad - Déficit Atención'},
	{type: 'Histeria'},
	{type: 'Inhibición'},
	{type: 'Inseguridad, Autoestima'},
	{type: 'Internet, vida online'},
	{type: 'Maltrato'},
	{type: 'Melancolía - Manía (Psicosis)'},
	{type: 'Miedo a Hablar en Público, Timidez'},
	{type: 'Mobbing (Acoso laboral)'},
	{type: 'Neurosis obsesiva'},
	{type: 'Orientación - Identidad sexual'},
	{type: 'Paranoia (Psicosis)'},
	{type: 'Pánico'},
	{type: 'Problemas de la Memoria'},
	{type: 'Programas de Psicoestimulación'},
	{type: 'Psicosis'},
	{type: 'Rehabilitación Daño Cerebral'},
	{type: 'Relaciones sociales'},
	{type: 'Relación de pareja'},
	{type: 'Tabaquismo. Dejar de Fumar'},
	{type: 'Trastorno Bipolar'},
	{type: 'Trastornos de la Personalidad'},
	{type: 'Trastornos del sueño - Insomnio'},
	{type: 'Trastornos enfermedad crónica'},
	{type: 'Trastornos Obsesivos Compulsivos'}
]

exports.language = [ 
	{type: 'Español'},
	{type: 'Inglés - English'},
	{type: 'Francés - Français'},
	{type: 'Alemán - Deutsch'},
	{type: 'Portugués - Português'},
	{type: 'Italiano'},
	{type: 'Lenguaje de signos (para SORDOS)'}
]

exports.health_insurance = [
	{type: 'AcaSalud'},
	{type: 'Accord Salud'},
	{type: 'AJB'},
	{type: 'Allianz - Ras'},
	{type: 'AMFEAFIP'},
	{type: 'AMFFA'},
	{type: 'AMTA'},
	{type: 'AMUR'},
	{type: 'ANDAR'},
	{type: 'APROSS'},
	{type: 'Bristol Medicine'},
	{type: 'COMEI'},
	{type: 'Conintem'},
	{type: 'CONSALUD'},
	{type: 'Dasmi'},
	{type: 'Daspu'},
	{type: 'DASUTeN'},
	{type: 'Docthos'},
	{type: 'DOSUBA'},
	{type: 'Federada Salud'},
	{type: 'Federada Salud'},
	{type: 'Femeba'},
	{type: 'FePRA'},
	{type: 'Galeno'},
	{type: 'Genesen'},
	{type: 'HCS Salud'},
	{type: 'Hercules'},
	{type: 'Integración Médica S.A.'},
	{type: 'IOMA'},
	{type: 'IOSE'},
	{type: 'Luis Pasteur'},
	{type: 'Maestranza'},
	{type: 'Mapfre'},
	{type: 'Medical Access'},
	{type: 'Medicus'},
	{type: 'Medifé'},
	{type: 'Mutual Federada 25 de Junio'},
	{type: 'Noble'},
	{type: 'Omint'},
	{type: 'OSBLYCA'},
	{type: 'OSDE'},
	{type: 'Osdop'},
	{type: 'OSFA'},
	{type: 'Osmecon'},
	{type: 'OSPE'},
	{type: 'Ospegap'},
	{type: 'OSPESCA'},
	{type: 'OSPF'},
	{type: 'OSPIA'},
	{type: 'OSPJN'},
	{type: 'OSPRA'},
	{type: 'OSPSA'},
	{type: 'OSSEG'},
	{type: 'PAMI'},
	{type: 'Premedic'},
	{type: 'PrenSalud'},
	{type: 'Qualitas'},
	{type: 'RAS'},
	{type: 'Reintegro Obras Sociales y Prepagas'},
	{type: 'SAC'},
	{type: 'SANCOR'},
	{type: 'SCIS'},
	{type: 'ServeSalud'},
	{type: 'Swiss Medical'},
	{type: 'UDOCBA'}
]
