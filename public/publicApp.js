angular.module('redpsiApp', [
    'ngRoute',
    'redpsiControllers',
    'redpsiServices'
])
    .config(['$routeProvider', 
        function($routeProvider) {
            $routeProvider

                .when('/', {
                    templateUrl : 'views/index.html',
                    controller  : 'mainController'
                });
    }]);