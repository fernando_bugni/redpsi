angular.module('redpsiServices',  [
    'ngResource'
])
    .factory('Psy', ['$resource',

        function($resource){
            return $resource('psy/', {}, {
                getAll: {method:'GET', isArray:true}
            });
        }
    ]);