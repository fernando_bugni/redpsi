angular.module('redpsiControllers', [])

    .controller('mainController', ['$scope', 'Psy',

        function($scope, Psy) {

            $scope.psys = Psy.getAll();
            $scope.currentTime = new Date();
        }
    ]);