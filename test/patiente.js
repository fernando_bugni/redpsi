var should = require('should');
var assert = require('assert');
var request = require('supertest');
var winston = require('winston');

var schemas = require('./../schemas');
var patiente = require('mongoose').model('patiente');

var url = 'http://localhost:3000';

describe('Patiente', function() {

	describe('getAll', function() {
		it('should return all the patiente', function(done) {
			request(url)
				.get('/pat')
				.end(function(err, res) {
					if (err) {
						throw err;
					}

					res.status.should.be.exactly(200);
					done();
				});
		});
	});

	describe('get', function() {

		var patid;
		
		before(function(done){
			patiente.create({'fb_id': 1}, function(err, pat) {
				if (err) 
					console.log("Can't create instance");
				console.log('Patiente created '+pat._id);
				patid = pat._id;
				done();
			});
		});

		it('should return a patiente', function(done) {
			request(url)
				.get('/pat/'+patid)
				.end(function(err, res) {
					if (err) {
						throw err;
					}
					res.status.should.be.exactly(200);
					assert(res.body.result._id == patid, "The id must be equal");
					done();	
				});
		});

		after(function(done){
			patiente.findByIdAndRemove(patid, function(err, pat) {
				if (err) 
					console.log("Can't delete the instance");
				console.log('Patiente deleted '+pat._id);
				done();
			});
		});
	});

	describe('post', function() {
		
		var patid;	

		it('should create a patiente', function(done) {
			request(url)
				.post('/pat')
				.send({'fb_id': 1})
				.end(function(err, res) {
					if (err) {
						throw err;
					}
					res.status.should.be.exactly(200);
					patid = res.body._id;
					done();	
				});
		});

		after(function(done){
			patiente.findByIdAndRemove(patid, function(err, pat) {
				if (err) 
					console.log("Can't delete the instance");
				console.log('Patiente deleted '+pat._id);
				done();
			});
		});
	});

	describe('put', function() {

		var patid;
		var fb_id = 1;
		var new_fb_id = 2;
		
		before(function(done){
			patiente.create({'fb_id': fb_id}, function(err, pat) {
				if (err) 
					console.log("Can't create instance");
				console.log('Patiente created '+pat._id);
				patid = pat._id;
				done();
			});
		});

		it('should return a patiente', function(done) {
			request(url)
				.put('/pat/'+patid)
				.send({'fb_id': new_fb_id})
				.end(function(err, res) {
					if (err) {
						throw err;
					}
					res.status.should.be.exactly(200);
					assert(res.body.fb_id == new_fb_id, "The id must be equal");
					assert(res.body._id == patid, "The id must be equal");
					done();	
				});
		});

		after(function(done){
			patiente.findByIdAndRemove(patid, function(err, pat) {
				if (err) 
					console.log("Can't delete the instance");
				console.log('Patiente deleted '+pat._id);
				done();
			});
		});
	});

	describe('delete', function() {

		var patid;
		
		before(function(done){
			patiente.create({'fb_id': 1}, function(err, pat) {
				if (err) 
					console.log("Can't create instance");
				console.log('Patiente created '+pat._id);
				patid = pat._id;
				done();
			});
		});

		it('should return a patiente', function(done) {
			request(url)
				.del('/pat/'+patid)
				.end(function(err, res) {
					if (err) {
						throw err;
					}
					res.status.should.be.exactly(200);
					assert(res.body._id == patid, "The id must be equal");
					done();	
				});
		});
	});
});