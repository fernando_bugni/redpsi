var should = require('should');
var assert = require('assert');
var request = require('supertest');
var winston = require('winston');

var schemas = require('./../schemas');
var psychologist = require('mongoose').model('psychologist');

var url = 'http://localhost:3000';

describe('Psychologist', function() {

	describe('getAll', function() {
		it('should return all the psychologist', function(done) {
			request(url)
				.get('/psy')
				.end(function(err, res) {
					if (err) {
						throw err;
					}

					res.status.should.be.exactly(200);
					done();
				});
		});
	});

	describe('get', function() {

		var psyid;
		
		before(function(done){
			psychologist.create({'fullname': 'test'}, function(err, psy) {
				if (err) 
					console.log("Can't create instance");
				console.log('Psychologist created '+psy._id);
				psyid = psy._id;
				done();
			});
		});

		it('should return a psychologist', function(done) {
			request(url)
				.get('/psy/'+psyid)
				.end(function(err, res) {
					if (err) {
						throw err;
					}
					res.status.should.be.exactly(200);
					assert(res.body.result._id == psyid, "The id must be equal");
					done();	
				});
		});

		after(function(done){
			psychologist.findByIdAndRemove(psyid, function(err, psy) {
				if (err) 
					console.log("Can't delete the instance");
				console.log('Psychologist deleted '+psy._id);
				done();
			});
		});
	});

	describe('post', function() {
		
		var psyid;	

		it('should create a psychologist', function(done) {
			request(url)
				.post('/psy')
				.send({'fullname': 'test'})
				.end(function(err, res) {
					if (err) {
						throw err;
					}
					res.status.should.be.exactly(200);
					psyid = res.body._id;
					done();	
				});
		});

		after(function(done){
			psychologist.findByIdAndRemove(psyid, function(err, psy) {
				if (err) 
					console.log("Can't delete the instance");
				console.log('Psychologist deleted '+psy._id);
				done();
			});
		});
	});

	describe('put', function() {

		var psyid;
		var fullname = 'test';
		var new_fullname = 'test1';
		
		before(function(done){
			psychologist.create({'fullname': fullname}, function(err, psy) {
				if (err) 
					console.log("Can't create instance");
				console.log('Psychologist created '+psy._id);
				psyid = psy._id;
				done();
			});
		});

		it('should return a psychologist', function(done) {
			request(url)
				.put('/psy/'+psyid)
				.send({'fullname': new_fullname})
				.end(function(err, res) {
					if (err) {
						throw err;
					}
					res.status.should.be.exactly(200);
					assert(res.body.fullname == new_fullname, "The id must be equal");
					assert(res.body._id == psyid, "The id must be equal");
					done();	
				});
		});

		after(function(done){
			psychologist.findByIdAndRemove(psyid, function(err, psy) {
				if (err) 
					console.log("Can't delete the instance");
				console.log('Psychologist deleted '+psy._id);
				done();
			});
		});
	});

	describe('delete', function() {

		var psyid;
		
		before(function(done){
			psychologist.create({'fullname': 'test'}, function(err, psy) {
				if (err) 
					console.log("Can't create instance");
				console.log('Psychologist created '+psy._id);
				psyid = psy._id;
				done();
			});
		});

		it('should return a psychologist', function(done) {
			request(url)
				.del('/psy/'+psyid)
				.end(function(err, res) {
					if (err) {
						throw err;
					}
					res.status.should.be.exactly(200);
					assert(res.body._id == psyid, "The id must be equal");
					done();	
				});
		});
	});
});