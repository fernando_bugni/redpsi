var data_bootstrap = require('./data_bootstrap');

var mongoose = require('mongoose');

console.log("setSchemas starts");


var db_name = "red"
mongodb_connection_string = 'mongodb://127.0.0.1:27017/' + db_name;

if(process.env.OPENSHIFT_MONGODB_DB_URL){
  mongodb_connection_string = process.env.OPENSHIFT_MONGODB_DB_URL + db_name;
}

mongoose.connect(mongodb_connection_string, function(error) {
    if (error) {
        console.log(error);
    }
});

var pat_schema = new mongoose.Schema({
    fb_id: Number,
    email: String
});

var patiente = mongoose.model('patiente', pat_schema);

///////////////////////////////////////////////////////////////////////////////////////////////////

var psychology_branch_schema = new mongoose.Schema({
    type: String
});

var psychology_branch = mongoose.model('psychology_branch', psychology_branch_schema);

var branches = data_bootstrap.branches;
for (var i = 0; i < branches.length; i++) {
    psychology_branch.update(branches[i], branches[i], {upsert: true}, function(err, psy) {
        if (err) console.log("Error creating psychology branch "+branches[i]);
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////

var psychology_patiente_age_schema = new mongoose.Schema({
    type: String
});

var psychology_patiente_age = mongoose.model('psychology_patiente_age', psychology_patiente_age_schema);

var patiente_age = data_bootstrap.patiente_age;
for (var i = 0; i < patiente_age.length; i++) {
    psychology_patiente_age.update(patiente_age[i], patiente_age[i], {upsert: true}, function(err, psy) {
        if (err) console.log("Error creating psychology branch "+patiente_age[i]);
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////

var psychology_patiente_type_schema = new mongoose.Schema({
    type: String
});

var psychology_patiente_type = mongoose.model('psychology_patiente_type', psychology_patiente_type_schema);

var patiente_type = data_bootstrap.patiente_type;
for (var i = 0; i < patiente_type.length; i++) {
    psychology_patiente_type.update(patiente_type[i], patiente_type[i], {upsert: true}, function(err, psy) {
        if (err) console.log("Error creating psychology branch "+patiente_type[i]);
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////

var psychology_treatment_schema = new mongoose.Schema({
    type: String
});

var psychology_treatment = mongoose.model('psychology_treatment', psychology_treatment_schema);

var patiente_treatment = data_bootstrap.treatment
for (var i = 0; i < patiente_treatment.length; i++) {
    psychology_treatment.update(patiente_treatment[i], patiente_treatment[i], {upsert: true}, function(err, psy) {
        if (err) console.log("Error creating psychology branch "+patiente_treatment[i]);
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////

var psychology_language_schema = new mongoose.Schema({
    type: String
});

var psychology_language = mongoose.model('psychology_language', psychology_language_schema);

var patiente_language = data_bootstrap.language
for (var i = 0; i < patiente_language.length; i++) {
    psychology_language.update(patiente_language[i], patiente_language[i], {upsert: true}, function(err, psy) {
        if (err) console.log("Error creating psychology branch "+patiente_language[i]);
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////

var psychology_health_insurance_schema = new mongoose.Schema({
    type: String
});

var psychology_health_insurance = mongoose.model('psychology_health_insurance', psychology_health_insurance_schema);

var health_insurance = data_bootstrap.health_insurance
for (var i = 0; i < health_insurance.length; i++) {
    psychology_health_insurance.update(health_insurance[i], health_insurance[i], {upsert: true}, function(err, psy) {
        if (err) console.log("Error creating psychology branch "+health_insurance[i]);
    });
}

///////////////////////////////////////////////////////////////////////////////////////////////////

var relationship_schema = new mongoose.Schema({
    psychologist: {type: mongoose.Schema.Types.ObjectId, ref: 'psychologist'},
    patiente: {type: mongoose.Schema.Types.ObjectId, ref: 'patiente'},
    start: Date,
    end: Date
});

var relationship = mongoose.model('relationship', relationship_schema);

var comment_schema = new mongoose.Schema({
    psychologist: {type: mongoose.Schema.Types.ObjectId, ref: 'psychologist'},
    patiente: {type: mongoose.Schema.Types.ObjectId, ref: 'patiente'},
    date: Date,
    text: String,
    stars: Number
});

var comment = mongoose.model('comment', comment_schema);

////////////////////////////////////////////////////////////////////////////////////////////////////

var psy_schema = new mongoose.Schema({
    fullname: String,
    surname: String,
    profile_image: {
        data: Buffer,
        contentType: String
    },
    matricula_number: Number,
    address: String,
    mobile_phone: Number,
    email: String,
    summary: String,
    degree: String, // datatype Lic. Dr. 
    university: String,
    specialization: [String],
    session_duration: Number,
    geo: {
        type: [Number],
        index: '2d'
    },
    price_individual: Number,
    social_medicine: String,
    fb_id: Number,
    patiente: [patiente],
    branches: [{type: mongoose.Schema.Types.ObjectId, ref: 'psychology_branch'}],
    patiente_age: {type: mongoose.Schema.Types.ObjectId, ref: 'psychology_patiente_age'},
    patiente_type: {type: mongoose.Schema.Types.ObjectId, ref: 'psychology_patiente_type'},
    treatment: {type: mongoose.Schema.Types.ObjectId, ref: 'psychology_treatment'},
    language: {type: mongoose.Schema.Types.ObjectId, ref: 'psychology_language'},
    health_insurance: {type: mongoose.Schema.Types.ObjectId, ref: 'psychology_health_insurance'}
});

var psychologist = mongoose.model('psychologist', psy_schema);

console.log("setSchemas finishes");
