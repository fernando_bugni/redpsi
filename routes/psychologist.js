var psychologist = require('mongoose').model('psychologist');

exports.getAll = function(req, res) {

	psychologist.find(function(err, psy, count) {

		if (err) res.json({
			error: err
		});
		res.json(psy);
	});
};

exports.get = function(req, res) {

	psychologist.findById(req.params.id, function(err, psy) {

		if (err) res.json({
			error: 'not found'
		});
		res.json({
			result: psy
		});
	});
};

exports.post = function(req, res) {

	psychologist.create(req.body, function(err, psy) {
		if (err) res.json({
			error: err
		});
		res.json(psy);
	});
};

exports.put = function(req, res) {

	psychologist.findByIdAndUpdate(req.params.id, req.body, function(err, psy) {
		if (err) res.json({
			error: err
		});
		res.json(psy);
	});
};

exports.delete = function(req, res) {

	psychologist.findByIdAndRemove(req.params.id, function(err, psy) {
		if (err) res.json({
			error: err
		});
		res.json(psy);
	});
};