var patiente = require('mongoose').model('patiente');

exports.getAll = function(req, res) {

	patiente.find(function(err, pat, count) {

		if (err) res.json({
			error: err
		});
		res.json({
			result: pat
		});
	});
};

exports.get = function(req, res) {

	patiente.findById(req.params.id, function(err, pat) {

		if (err) res.json({
			error: 'not found'
		});
		res.json({
			result: pat
		});
	});
};

exports.post = function(req, res) {

	patiente.create(req.body, function(err, pat) {
		if (err) res.json({
			error: err
		});
		res.json(pat);
	});
};

exports.put = function(req, res) {

	patiente.findByIdAndUpdate(req.params.id, req.body, function(err, pat) {
		if (err) res.json({
			error: err
		});
		res.json(pat);
	});
};

exports.delete = function(req, res) {

	patiente.findByIdAndRemove(req.params.id, function(err, pat) {
		if (err) res.json({
			error: err
		});
		res.json(pat);
	});
};