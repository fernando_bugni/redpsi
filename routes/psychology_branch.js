var psychology_branch = require('mongoose').model('psychology_branch');

exports.getAll = function(req, res) {

	psychology_branch.find(function(err, brch, count) {

		if (err) res.json({
			error: err
		});
		res.json({
			result: brch
		});
	});
};