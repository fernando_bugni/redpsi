var psychology_treatment = require('mongoose').model('psychology_treatment');

exports.getAll = function(req, res) {

	psychology_treatment.find(function(err, treatment, count) {

		if (err) res.json({
			error: err
		});
		res.json({
			result: treatment
		});
	});
};