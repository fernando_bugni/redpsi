var psychology_language = require('mongoose').model('psychology_language');

exports.getAll = function(req, res) {

	psychology_language.find(function(err, lang, count) {

		if (err) res.json({
			error: err
		});
		res.json({
			result: lang
		});
	});
};