var psychology_patiente_type = require('mongoose').model('psychology_patiente_type');

exports.getAll = function(req, res) {

	psychology_patiente_type.find(function(err, type, count) {

		if (err) res.json({
			error: err
		});
		res.json({
			result: type
		});
	});
};