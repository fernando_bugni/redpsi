var psychology_patiente_age = require('mongoose').model('psychology_patiente_age');

exports.getAll = function(req, res) {

	psychology_patiente_age.find(function(err, pat_age, count) {

		if (err) res.json({
			error: err
		});
		res.json({
			result: pat_age
		});
	});
};