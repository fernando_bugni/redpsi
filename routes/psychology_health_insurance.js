var psychology_health_insurance = require('mongoose').model('psychology_health_insurance');

exports.getAll = function(req, res) {

	psychology_health_insurance.find(function(err, hi, count) {

		if (err) res.json({
			error: err
		});
		res.json({
			result: hi
		});
	});
};