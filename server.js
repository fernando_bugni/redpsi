var express = require('express');
var mongoose = require('mongoose');
var path = require('path');
var errorHandler = require('errorhandler');
var bodyParser = require('body-parser');

//Setting mongoDB schemas
var schemas = require('./schemas');

var index = require('./routes/index');
var psychologist = require('./routes/psychologist');
var patiente = require('./routes/patiente');
var psychology_branch = require('./routes/psychology_branch');
var psychology_patiente_age = require('./routes/psychology_patiente_age');
var psychology_patiente_type = require('./routes/psychology_patiente_type');
var psychology_treatment = require('./routes/psychology_treatment');
var psychology_language = require('./routes/psychology_language');
var psychology_health_insurance = require('./routes/psychology_health_insurance');

var app = express();

//Setting server
//app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, 'public')));

//Parse body and params
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// Api
app.get('/psy/branches', psychology_branch.getAll);
app.get('/psy/patiente-age', psychology_patiente_age.getAll);
app.get('/psy/patiente-type', psychology_patiente_type.getAll);
app.get('/psy/treatment', psychology_treatment.getAll);
app.get('/psy/language', psychology_language.getAll);
app.get('/psy/health_insurance', psychology_health_insurance.getAll);
app.put('/psy/:id', psychologist.put);
app.del('/psy/:id', psychologist.delete);
app.get('/psy/:id', psychologist.get);
app.get('/psy', psychologist.getAll);
app.post('/psy', psychologist.post);

app.get('/pat', patiente.getAll);
app.get('/pat/:id', patiente.get);
app.post('/pat', patiente.post);
app.put('/pat/:id', patiente.put);
app.del('/pat/:id', patiente.delete);

// Webpages
app.use('/', express.static(__dirname + '/public/views'));

if ('development' == app.get('env')) {
    app.use(errorHandler());
}

var server_port = process.env.OPENSHIFT_NODEJS_PORT || 8080
var server_ip_address = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

app.listen(server_port, server_ip_address, function() {
//    console.log('Express server listening on port ' + app.get('port'));
      console.log( "Listening on " + server_ip_address + ", port " + server_port)
});
